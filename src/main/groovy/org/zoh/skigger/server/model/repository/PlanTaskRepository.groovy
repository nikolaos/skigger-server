package org.zoh.skigger.server.model.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.zoh.skigger.server.model.domain.PlanTask

/**
 * Created by nikolay on 15.10.14.
 */
@RepositoryRestResource(path = "planTask")
interface PlanTaskRepository extends CrudRepository<PlanTask, String> {

}