package org.zoh.skigger.server.model.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.zoh.skigger.server.model.domain.PlanAttempt

/**
 * Created by nikolay on 15.10.14.
 */
@RepositoryRestResource(path='planAttempt')
interface PlanAttemptRepository extends CrudRepository<PlanAttempt, String> {

}