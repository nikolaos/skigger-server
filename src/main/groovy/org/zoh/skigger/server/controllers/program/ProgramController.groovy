package org.zoh.skigger.server.controllers.program

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Created by n_ushmodin on 22.10.14.
 */
@Controller
@RequestMapping('/program')
class ProgramController {
    @Value('${spring.thymeleaf.cache}')
    String cache

    @RequestMapping('index')
    void index() {
        int a = 1
    }
}
