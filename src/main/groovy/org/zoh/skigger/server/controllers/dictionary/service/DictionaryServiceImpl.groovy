package org.zoh.skigger.server.controllers.dictionary.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.stereotype.Component
import org.zoh.skigger.server.controllers.dictionary.model.Attempt
import org.zoh.skigger.server.controllers.dictionary.model.Program
import org.zoh.skigger.server.controllers.dictionary.model.Task
import org.zoh.skigger.server.controllers.dictionary.model.Workout
import org.zoh.skigger.server.controllers.dictionary.service.DictionaryService
import org.zoh.skigger.server.model.repository.ProgramRepository

import javax.transaction.Transactional

/**
 * Created by nikolay on 16.10.14.
 */
@Service
@Transactional
class DictionaryServiceImpl implements DictionaryService{
    @Autowired
    ProgramRepository programRepository

    List<Program> getAllPrograms() {
        programRepository.findAll().collect { p ->
            def workouts = p.workouts.collect({ w ->
                def tasks = w.tasks.collect({ t->
                    def attempts = t.attempts.collect({ a->
                        new Attempt(id:a.id.toString(), seq: a.seq, count: a.count, amount: a.amount, delay: a.delay)
                    })
                    new Task(id: t.id.toString(), name: t.name, seq: t.seq, attempts: attempts)
                })
                new Workout(id: w.id.toString(), name: w.name, seq: w.seq, tasks: tasks)
            })
            new Program(id:p.id, name:p.name, workouts: workouts)
        }
    }
}
