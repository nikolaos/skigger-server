package org.zoh.skigger.server.controllers.dictionary.adapter

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.zoh.skigger.server.controllers.dictionary.model.Program
import org.zoh.skigger.server.controllers.dictionary.service.DictionaryService
import org.zoh.skigger.server.json.JsonResponse

/**
 * Created by nikolay on 16.10.14.
 */
@Component
class DictionaryAdapter {

    @Autowired
    DictionaryService dictionaryService

    def getAllPrograms() {
        new JsonResponse<>(body: dictionaryService.allPrograms)
    }
}
