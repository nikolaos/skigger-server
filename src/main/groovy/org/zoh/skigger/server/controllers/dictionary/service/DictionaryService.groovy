package org.zoh.skigger.server.controllers.dictionary.service

import org.zoh.skigger.server.controllers.dictionary.model.Program

/**
 * Created by nikolay on 16.10.14.
 */

interface DictionaryService {
    List<Program> getAllPrograms()
}