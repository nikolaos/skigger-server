package org.zoh.skigger.server.controllers.dictionary

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.zoh.skigger.server.controllers.dictionary.adapter.DictionaryAdapter
import org.zoh.skigger.server.controllers.dictionary.service.DictionaryService

/**
 * Created by nikolay on 15.10.14.
 */
@RestController
@RequestMapping('/dictionary')
class DictionaryController  {
    @Autowired
    DictionaryAdapter adapter

    @RequestMapping('')
    def index() {
        adapter.allPrograms
    }
}
