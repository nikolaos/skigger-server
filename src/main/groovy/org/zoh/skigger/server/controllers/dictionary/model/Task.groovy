package org.zoh.skigger.server.controllers.dictionary.model

/**
 * Created by nikolay on 15.10.14.
 */
class Task {
    String id
    String name
    int seq
    List<Attempt> attempts
}
