package org.zoh.skigger.server.json

/**
 * Created by nikolay on 16.10.14.
 */
class JsonResponse<T> {
    T body
    boolean error = false
    String errorMessage
}
